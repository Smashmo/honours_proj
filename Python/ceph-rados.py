#!/usr/bin/env python2.7
import rados
import pprint as pp

class Core():
    def __init__(self):
        self.pool = "rbd"

    def open_cluster(self):
        cluster = rados.Rados(conffile='/etc/ceph/ceph.conf')
        cluster.connect()
        print "\nCluster Statistics"
        print "------------------"
        print "\nCluster ID: " + cluster.get_fsid()

        cluster_stats = cluster.get_cluster_stats()
        for key, value in cluster_stats.iteritems():
            if key == 'kb':
                print "Total Space: "+ str(value/1024/1024) + "GB"
            elif key == 'kb_used':
                print "Space Used: "+ str(value/1024/1024) + "GB"
            elif key == 'kb_avail':
                print "Free Space avalable: "+ str(value/1024/1024) + "GB"
            else:
                print key,value
        pp.pprint(cluster.list_pools())
        #pools = cluster.list_pools()
        #for pool in pools:
        with cluster.open_ioctx('rbd') as ioctx:
            obj_iter = ioctx.list_objects()
            while True:
                try:
                    rados_obj = obj_iter.next()
                    s_ioctx, s_key = str(rados_obj).strip(')').split(',')
                    print "\nRados Object Name(key): "+s_key.strip('key=')
                    print "Size(Bytes) : "+str(rados_obj.stat()[0])
                    print "TimeStamp: "+ str(rados_obj.stat()[1])
                except StopIteration:
                    break
        cluster.shutdown()


if __name__ == "__main__":
    zoid = Core()
    zoid.open_cluster()
