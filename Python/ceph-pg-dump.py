#!/bin/env python

import json
import pprint as pp
from subprocess import *

class Core():
    def __init__(self):
        pass

    def pg_dump(self):
        ceph_comand = ['ceph', 'pg', 'dump', '--format', 'json']
        pg_info = Popen(ceph_comand, stdout=PIPE)
        stdout = pg_info.communicate()
        for line in stdout:
            if (line != None):
                pg_out = line.split('\n')
                pg_json = json.loads(pg_out[1])
                print json.dumps(pg_json, indent=2)
if __name__ == "__main__":
    zoid = Core()
    zoid.pg_dump()
