#!/usr/bin/env python2.7
import rados
import rbd
import pprint as pp

class Core():
    def __init__(self):
	pass

    def rbd_image_stat(self):
        cluster = rados.Rados(conffile='/etc/ceph/ceph.conf')
        cluster.connect()
        pools = cluster.list_pools()

        try:
            for pool in pools:
                with cluster.open_ioctx(pool) as ioctx:
                    rbd_inst = rbd.RBD()
                    print "\nList of images in " + pool +": " + str(rbd_inst.list(ioctx))
                    if rbd_inst.list(ioctx):
                        for image in rbd_inst.list(ioctx):
                            print "Lookign into : "+ str(rbd_inst.list(ioctx))
                            with rbd.Image(ioctx, image) as pool_image:
                                print "\nReading on: "+image
                                print "------------------"
                                file_name = image+".rbd"
                                image_size_first = pool_image.size()/2
                                print image_size_first
                                f = open(file_name,'wb')
                                f.write(pool_image.read(0,image_size_first))
                                f.close()
                                f = open(file_name+"2",'wb')
                                f.write(pool_image.read(image_size_first,pool_image.size()))
                                f.close()
                    else:
                        print "No images in: "+pool+"\n"
        finally:
            cluster.shutdown()

if __name__ == "__main__":
    zoid = Core()
    zoid.rbd_image_stat()
