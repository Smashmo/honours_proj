#!/usr/bin/env python2.7
import rados
import rbd
import pprint as pp

class Core():
    def __init__(self):
	pass

    def rbd_image_stat(self):
        cluster = rados.Rados(conffile='/etc/ceph/ceph.conf')
        cluster.connect()
        pools = cluster.list_pools()

        try:
            for pool in pools:
                with cluster.open_ioctx(pool) as ioctx:
                    rbd_inst = rbd.RBD()
                    print "\nList of images in " + pool +": " + str(rbd_inst.list(ioctx))
                    if rbd_inst.list(ioctx):
                        for image in rbd_inst.list(ioctx):
                            print "Lookign into : "+ str(rbd_inst.list(ioctx))
                            with rbd.Image(ioctx, image) as pool_image:
                                print "\nStats on: "+image
                                print "------------------"
                                pp.pprint(pool_image.stat())
                                print "\nExtened Stats"
                                print "------------------"
                                print "Stripe unit: "+str(pool_image.stripe_unit())
                                print "Stripe count: "+str(pool_image.stripe_count())
                                print "\nImage Snapshots"
                                print "------------------"
                                if pool_image.list_snaps():
                                    pp.pprint(pool_image.list_snaps().__dict__)
                                else:
                                    print "\nNo snapshots of :"+image
                                print "\nLockers of: "+image
                                print "------------------"
                                if pool_image.list_lockers():
                                    pp.pprint(pool_image.list_lockers())
                                else:
                                    print "No clients locking image."
                    else:
                        print "No images in: "+pool+"\n"
        finally:
            cluster.shutdown()

if __name__ == "__main__":
    zoid = Core()
    zoid.rbd_image_stat()
