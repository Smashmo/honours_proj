#include <stdio.h>
#include <stdlib.h>
#include <rados/librados.h>
#include <rbd/librbd.h>





int main(int argc, char** argv){
  printf("Trying to connect to cluster..\n");
  int err;
  rados_t cluster;

  err = rados_create(&cluster, NULL);  
  if (err < 0) {
    fprintf(stderr, "%s: cannot creaste a cluster handle: %s\n", argv[0], strerror(-err));
    exit(1);
  }
  
  err = rados_conf_read_file(cluster, "/etc/ceph/ceph.conf");
  if (err < 0) {
    fprintf(stderr, "%s: cannot read config file: %s\n", argv[0], strerror(-err));
    exit(1);
  }

  err = rados_connect(cluster);
  if (err < 0) {
    fprintf(stderr, "%s: cannot connect to cluster: %s\n", argv[0], strerror(-err));
    exit(1);
  }
  
  rados_ioctx_t io;
  char *poolname = "zoid";

  err = rados_ioctx_create(cluster, poolname, &io);
  if (err < 0) {
    fprintf(stderr, "%s: cannot open rados pool %s: %s\n", argv[0], poolname, strerror(-err));
    rados_shutdown(cluster);
    exit(1);
  }
  
  
  struct rados_cluster_stat_t results;
  err = rados_cluster_stat(cluster, &results);
  if (err < 0) {
    fprintf(stderr, "%s: cannot stat cluster %s: %s\n", argv[0], poolname, strerror(-err));
    rados_shutdown(cluster);
    exit(1);
  }
  printf("KB used: %lu\n", results.kb_used);
  return 0;
}
